let colores = new Array(3);
let recursoSeleccionado;
let disponible;
let edificio;
let edificioCreado=false;
let matriz = new Array(4);
matriz [0] = [0,0,0,0];
matriz [1] = [0,0,0,0];
matriz [2] = [0,0,0,0];
matriz [3] = [0,0,0,0];
let matrizCeldaSeleccionada = new Array(2);
let matrizPosicion;
let blnGameOver=true;
let blnStartGame=false;
let monuCatedral=false;
let monuPalacio=false;

function jugar(boton){
    if(blnGameOver==true){
            if(matriz[0][0]!=0){
                location.reload();
            }else{
            blnGameOver = !blnGameOver;
            alert("Para comenzar, elige el monumento que usarás para esta partida. Recuerda que no lo podrás cambiar más adelante");
            }
    }
}

function mostrarRecursos(){
    let recurso;
    let numerornd;
    recursoSeleccionado=null;
    for(let i=1; i<=3; i++){
         numerornd = Math.round(Math.random()*(5-1)+parseInt(1));
         switch(numerornd){
             case 1:
                 recurso="#804000";
                 break;
             case 2:
                 recurso="#F0F715";
                 break;
             case 3: 
                 recurso="#DF2317";
                 break;
             case 4:
                 recurso="#17D9DF";
                 break;
             case 5:
                 recurso="#939393"
                 break;
         }
        colores[i-1] = numerornd;
        document.getElementById('button'+i).style.backgroundColor=recurso;
    }
}

function seleccion(boton){
    if(monuCatedral == false && monuPalacio ==false && blnGameOver==false){
        alert("Elige un monumento");
    }else
    if(blnGameOver==true){
        alert("Presiona iniciar juego");
    }else{
        recursoSeleccionado = colores[boton.value-1];
    }
}

function colocarRecurso(celda){
    disponible = true;
    let urlrecurso;
    switch(recursoSeleccionado){
        case 1:
            urlrecurso = "assets/imagenes/recurso1.png";
            break;
        case 2:
            urlrecurso = "assets/imagenes/recurso2.png";
            break;
        case 3:
            urlrecurso = "assets/imagenes/recurso3.png";
            break;
        case 4:
            urlrecurso = "assets/imagenes/recurso4.png";
            break;
        case 5:
            urlrecurso = "assets/imagenes/recurso5.png";
            break;
    }
    if(urlrecurso!=null){
        guardarMatriz(celda);
            if(disponible==true){
                celda.src=urlrecurso;
                comprobarEdificios();
                if(edificioCreado==false){
                    gameOver();
                }
                if(edificioCreado==false && blnGameOver==false){
                    recursoSeleccionado=null;
                    mostrarRecursos(); 
                }
            }
    }
}

function guardarMatriz(imagen){
    let columna = "c";
    let celda;
    for(let i=0; i<=3; i++){
        for(let j=0; j<=3; j++){
            celda = columna+i+j;
            if(celda == imagen.id){
                if(matriz[i][j] == 0){
                    matriz [i][j] = recursoSeleccionado;
                    matrizCeldaSeleccionada[0] = i;
                    matrizCeldaSeleccionada[1] = j;
                    disponible=true;
                }else{
                    alert("No puedes colocar dos recursos en el mismo cuadro");
                    disponible=false;
                } 
            }
        }
    }
}

function comprobarEdificios(){
    switch(recursoSeleccionado){
        case 1:
            palacio();
            granja();
            salaBanquete();
            pozo();
            break;
        case 2:
            catedral();
            palacio();
            cabania();
            granja();
            posada();
            break;
        case 3: 
            palacio();
            cabania();
            taverna();
            break;
        case 4:
            catedral();
            palacio();
            cabania();
            taverna();
            asilo();
            salaBanquete();
            posada();
            break;
        case 5:
            catedral();
            asilo();
            posada();
            pozo();
            break;
    }  
}

function llamarDosFunciones(celda){
    if(blnGameOver==true){
        alert("Presiona iniciar juego");
    }else if(blnGameOver==false && monuCatedral==false && monuPalacio==false){
        alert("Elige un monumento");
    }else{
        if(recursoSeleccionado==null){
            alert("Elige un recurso");
        }else if(recursoSeleccionado<=5){
                colocarRecurso(celda);
        }else if(recursoSeleccionado>5){
             colocarEdificio(celda);
        }
    }
}

function creacionEdificio(){
    let mensaje = confirm("Acabas de crear: "+edificio+"\n ¿Deseas crear el edificio?");
    if (mensaje){
        edificioCreado = true;
    }else{
        recursoSeleccionado = null;
        edificioCreado=false;
    }
}

function colocarEdificio(lugar){
    let urlrecurso;
    switch(recursoSeleccionado){
        case 6:
            urlrecurso = "assets/imagenes/cabaña.png";
            break;
        case 7:
            urlrecurso = "assets/imagenes/granja.png";
            break;
        case 8:
            urlrecurso = "assets/imagenes/taberna.png";
            break;
        case 9:
            urlrecurso = "assets/imagenes/posada.png";
            break;
        case 10:
            urlrecurso = "assets/imagenes/salabanquete.png";
            break;
        case 11:
            urlrecurso = "assets/imagenes/asilo.png";
            break;
        case 12:
            urlrecurso = "assets/imagenes/pozo.png";
            break;
        case 13:
            urlrecurso = "assets/imagenes/piedramolino.png";
            break;
        case 14:
            urlrecurso = "assets/imagenes/cobertizo.png";
            break;
        case 15:
            urlrecurso = "assets/imagenes/fuente.png";
            break;
        case 20:
            urlrecurso = "assets/imagenes/monuCatedral.png";
            break;
        case 21:
            urlrecurso = "assets/imagenes/monuPalacio.png";
            break;
    }
    guardarMatrizEdificio(lugar);
            if(disponible==true){
                lugar.src=urlrecurso;
                recursoSeleccionado=null;
                edificioCreado = false;
                urlrecurso = null;
                gameOver();
                if(blnGameOver==false){
                mostrarRecursos();                     
                }
            }
}

function guardarMatrizEdificio(lugar){
    let comparacion;
    disponible=false;
    for(let m=0; m<matrizPosicion.length; m++){
        if(lugar.id == 'c'+matrizPosicion[m][0]+matrizPosicion[m][1]){
            disponible=true;
        }
    }
    if(disponible==true){
        for(let m=0; m<matrizPosicion.length; m++){
            comparacion = 'c'+matrizPosicion[m][0]+matrizPosicion[m][1];
            if(lugar.id == comparacion){
                matriz[matrizPosicion[m][0]][matrizPosicion[m][1]] = recursoSeleccionado;
            }else{
                    matriz[matrizPosicion[m][0]] [matrizPosicion[m][1]] = 0;
                    document.getElementById('c'+matrizPosicion[m][0]+matrizPosicion[m][1]).src='assets/imagenes/board.png';
                }
        }
    }else{
        alert("Selecciona una casilla válida");
    }
}

function cabania(){
if(edificioCreado == false){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];
    matrizPosicion = new Array(3);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    let matrizRecurso = new Array(3);
    matrizRecurso [0] = recursoSeleccionado; 

    let n;
    if(x==0){
        n=x;
    }else{
        n=x-1;
    }
    let m;
    if(x==3){
        m=3;
    }else{
        m=x+1;
    }
    
    if(matrizRecurso[0] != 4){
      for(let k=n; k<=m; k++){
            if(k==x){
                k++;
            }
        if(k<4){  
        if(matriz[k][y]==2 || matriz[k][y]==3 || matriz[k][y]==4){
            matrizPosicion[1][0] = k;
            matrizPosicion[1][1] = y;
            matrizRecurso [1] = matriz[k][y];
            for(let l=y-1; l<=y+1; l++){
                if(l==y){
                    l++;
                }
                if((matriz[k][l] == 2 || matriz[k][l] == 3 || matriz[k][l] == 4) && edificioCreado==false){
                    matrizPosicion[2][0] = k;
                    matrizPosicion[2][1] = l;
                    matrizRecurso[2] = matriz[k][l];
                    if((matrizRecurso[0]==3 || matrizRecurso[0]==2) && matrizRecurso[1]==4 && (matrizRecurso[2]==2 || matrizRecurso[2]==3) 
                      && matrizRecurso[2]!=matrizRecurso[0]){
                         edificio = "cabaña";
                         recursoSeleccionado = 6;
                         creacionEdificio();
                    }   
                }
            } 
        }
        
        if(k==x+1 && edificioCreado == false){
            for(let l=y-1; l<=y+1; l++){
                if(l==y){
                    l++;
                }
                if(matriz[x][l] == 2 || matriz[x][l] == 3 || matriz[x][l] == 4){
                    matrizPosicion[1][0] = x;
                    matrizPosicion[1][1] = l;
                    matrizRecurso[1] = matriz[x][l];
                    for(let p=n; p<=x+1; p++){
                        if(p==x){
                            p++;
                        }
                        if((matriz[p][l] == 2 || matriz[p][l] == 3 || matriz[p][l] == 4) && edificioCreado==false){
                            matrizPosicion[2][0] = p;
                            matrizPosicion[2][1] = l;
                            matrizRecurso[2] = matriz[p][l];
                            if((matrizRecurso[0]==3 || matrizRecurso[0]==2) && matrizRecurso[1]==4 && (matrizRecurso[2]==2 ||matrizRecurso[2]==3) && matrizRecurso[2]!=matrizRecurso[0]){
                                 edificio = "cabaña";
                                 recursoSeleccionado = 6;
                                 creacionEdificio();
                            } 

                        }
                    }
                }
            }
        }
       }
      }
    }
    else{        
        for(let k=n; k<=m; k++){
            for(let l=y-1; l<=y+1; l++){
                if( (k==x-1 && l==y) || (k==x && l==y-1) || (k==x && l==y+1) || (k==x+1 && l==y)  ){
                    if(matriz[k][l] == 2 || matriz[k][l] == 3){
                        matrizPosicion[1][0] = k;
                        matrizPosicion[1][1] = l;
                        matrizRecurso[1] = matriz[k][l];
                        if(k!=x){
                            for(let p=y-1; p<=y+1; p++){
                                if((matriz[x][p] == 2 || matriz[x][p]==3)  && p!=y){
                                    matrizPosicion[2][0] = x;
                                    matrizPosicion[2][1] = p;
                                    matrizRecurso[2] = matriz[x][p];
                                    if(matrizRecurso[1] != matrizRecurso[2]){
                                        edificio = "cabaña";
                                        recursoSeleccionado = 6;
                                        creacionEdificio();
                                        l=y+2;
                                        k=x+2;
                                    }
                                }
                            }
                        }else{
                            for(let p=n; p<=m; p++){
                                if((matriz[p][y] == 2 || matriz[p][y] == 3) && p!=x && edificioCreado==false){
                                    matrizPosicion[2][0] = p;
                                    matrizPosicion[2][1] = y;
                                    matrizRecurso[2] = matriz[p][y];
                                    if(matrizRecurso[1] != matrizRecurso[2]){
                                        edificio = "cabaña";
                                        recursoSeleccionado = 6;
                                        creacionEdificio();   
                                        l=y+2;
                                        k=x+2;
                                    }
                                }
                            }
                          } 
                        }
                    }
                }
        }
    }
}
}

function granja(){
if(edificioCreado==false){
    //"X y "Y" guardan la posición de la casilla donde se insertó el recurso
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];
    
    //guardar la posición en que se encuentra el recurso para formar la granja
    matrizPosicion = new Array(4);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    matrizPosicion [3] = [0,0];
    
    //guardar el valor del recurso
    let matrizRecurso = new Array(4);
    matrizRecurso [0] = recursoSeleccionado; 

    //determina si los recursos están acomodados de manera correcta
    let posicionCorrecta = false;
    //determina la suma del valor de los recursos
    let suma=0;
    
    for(let k=y-1; k<=y+1; k++){
        if(k==-1){
            k=1;
        }
        
        if(k==y){
            k++;
        }
        if(matriz[x][k] == 1 || matriz[x][k] == 2){
            matrizPosicion [1][0] = x;
            matrizPosicion [1][1] = k;
            matrizRecurso [1] = matriz[x][k];
            let n;
            if(x==0){
                n=x;
            }else{
                n=x-1;
            }
            let m;
            if(x==3){
                m=3;
            }else{
                m=x+1;
            }
            
            for(let l=n; l<=m; l++){
                if(l==x){
                        l++;
                    }
                if(l==-1){
                    l==1;
                }
            if(l<4){           
                if(matriz[l][k] == 1 || matriz[l][k] == 2){
                    matrizPosicion[2][0] = l;
                    matrizPosicion[2][1] = k;
                    matrizRecurso [2] = matriz[l][k];
                    if(matriz[l][y]==1 || matriz[l][y]==2){
                        matrizPosicion[3][0] = l;
                        matrizPosicion[3][1] = y;
                        matrizRecurso[3] = matriz[l][y];
                        
                        for(let m=0; m<=3 ; m++){
                            suma += matrizRecurso[m];
                        }
                        
                        if(matrizRecurso[1] == matrizRecurso[0] || matrizRecurso[3] == matrizRecurso[0]){
                            posicionCorrecta=true;
                        }
                            
                        if(suma==6 && posicionCorrecta==true){
                            edificio = "granja";
                            recursoSeleccionado = 7;
                            creacionEdificio();
                            suma=0;
                            posicionCorrecta=false;
                        }
                    }
                }
            }
             }      
           }
    }
 }
}

function posada(){
if(edificioCreado == false){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];    
    matrizPosicion = new Array(3);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    
    let matrizRecurso = new Array(3);
    matrizRecurso[0] = recursoSeleccionado;
    let suma=0;
    let n;
    if(x==0){
        n=x;
    }else{
        n=x-1;
    }
    let m;
    if(x==3){
        m=3;
    }else{
        m=x+1;
    }
    
    for(let k=n; k<=m; k++){
        for(let l=y-1; l<=y+1; l++){
            if((k==x-1 && l==y) || (k==x && l!=y) || (k==x+1 && l==y)){
                if(matriz[k][l] == 2 || matriz[k][l] == 4 || matriz[k][l] == 5){
                    matrizPosicion[1][0] = k;
                    matrizPosicion[1][1] = l;
                    matrizRecurso[1] = matriz[k][l]; 
                    if(k==x){
                        let r;
                        if(y<2){
                            r=0;
                        }else{
                            r=y-2;
                        }
                        for(let m=r; m<=y+2; m++){
                            if((matriz[x][m] == 2 || matriz[x][m] == 4 || matriz[x][m] == 5) && (m!=l && m!=y) && (m!=l-3 && m!=l+3)){
                                matrizPosicion[2][0] = x;
                                matrizPosicion[2][1] = m;
                                matrizRecurso[2] = matriz[x][m];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 11 && ((matrizRecurso[0]==2 || matrizRecurso[2]==2)
                                                                                                    && (matrizRecurso[0]==4 || matrizRecurso[2]==4))){
                                    l=y+2;
                                    edificio="posada";
                                    recursoSeleccionado = 9;
                                    creacionEdificio();
                                }
                            }
                        }
                    }
                    if(k!=x){
                        let n;
                        if(x<2){
                            n=0;
                        }else{
                            n=x-2
                        }                        
                        for(let m=n; m<=x+2; m++){
                            if(m<4){
                            if((matriz[m][y] == 2 || matriz[m][y] == 4 || matriz[m][y] == 5) && (m!=k && m!=x) && (m!=k-3 && m!=k+3)){
                                matrizPosicion[2][0] = m;
                                matrizPosicion[2][1] = y;
                                matrizRecurso[2] = matriz[m][y];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 11 && (matrizRecurso[0]==2 || matrizRecurso[2] ==2)
                                                                                                   ){
                                    edificio = "posada";
                                    recursoSeleccionado = 9;
                                    creacionEdificio();
                                    l=y+2;
                                }
                            }
                        }
                        }
                    }
                }
            } 
        }
    }
 }
}

function salaBanquete(){
if(edificioCreado == false){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];    
    matrizPosicion = new Array(3);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    let matrizRecurso = new Array(3);
    matrizRecurso[0] = recursoSeleccionado;
    let suma=0;
    
    let n;
    if(x==0){
        n=x;
    }else{
        n=x-1;
    }
    let m;
    if(x==3){
        m=3;
    }else{
        m=x+1;
    }
     
    for(let k=n; k<=m; k++){
        for(let l=y-1; l<=y+1; l++){
            if((k==x-1 && l==y) || (k==x && l!=y) || (k==x+1 && l==y)){
                if(matriz[k][l]==1 || matriz[k][l]==4){
                    matrizPosicion[1][0] = k;
                    matrizPosicion[1][1] = l;
                    matrizRecurso[1] = matriz[k][l]; 
                    if(k==x){
                        let r;
                        if(y<2){
                            r=0;
                        }else{
                            r=y-2;
                        }
                        for(let m=r; m<=y+2; m++){
                            if((matriz[x][m] == 1 || matriz[x][m] == 4) && (m!=l && m!=y) && (m!=l-3 && m!=l+3)){
                                matrizPosicion[2][0] = x;
                                matrizPosicion[2][1] = m;
                                matrizRecurso[2] = matriz[x][m];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 6 &&(matrizRecurso[0]==4 || matrizRecurso[2]==4)){
                                    l=y+2;
                                    edificio="sala de banquetes";
                                    recursoSeleccionado = 10;
                                    creacionEdificio();
                                }
                            }
                        }
                    }
                    if(k!=x){
                        let n;
                        if(x<2){
                            n=0;
                        }else{
                            n=x-2
                        }                        
                        for(let m=n; m<=x+2; m++){
                            //alert(m+" "+y);
                            if(m<4){
                            if((matriz[m][y] == 1 || matriz[m][y] == 4) && (m!=k && m!=x) && (m!=k-3 && m!=k+3)){
                                matrizPosicion[2][0] = m;
                                matrizPosicion[2][1] = y;
                                matrizRecurso[2] = matriz[m][y];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 6 &&(matrizRecurso[0]==4 || matrizRecurso[2]==4)){
                                    edificio = "sala de banquetes";
                                    recursoSeleccionado = 10;
                                    creacionEdificio();
                                    l=y+2;
                                }
                            }
                          }
                        }
                    }
                }
            } 
        }
    }
 }
}

function asilo(){
if(edificioCreado == false){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];    
    matrizPosicion = new Array(3);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    let matrizRecurso = new Array(3);
    matrizRecurso[0] = recursoSeleccionado;
    let suma=0;
    
    let n;
    if(x==0){
        n=x;
    }else{
        n=x-1;
    }
    let t;
    if(x==3){
        t=3;
    }else{
        t=x+1;
    }
     
    for(let k=n; k<=t; k++){
        for(let l=y-1; l<=y+1; l++){
            if((k==x-1 && l==y) || (k==x && l!=y) || (k==x+1 && l==y)){
                if(matriz[k][l]==5 || matriz[k][l]==4){
                    matrizPosicion[1][0] = k;
                    matrizPosicion[1][1] = l;
                    matrizRecurso[1] = matriz[k][l]; 
                    if(k==x){
                        let r;
                        if(y<2){
                            r=0;
                        }else{
                            r=y-2;
                        }
                        for(let m=r; m<=y+2; m++){
                            if((matriz[x][m] == 5 || matriz[x][m] == 4) && (m!=l && m!=y) && (m!=l-3 && m!=l+3)){
                                matrizPosicion[2][0] = x;
                                matrizPosicion[2][1] = m;
                                matrizRecurso[2] = matriz[x][m];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 14 &&(matrizRecurso[0]==4 || matrizRecurso[2]==4)){
                                    l=y+2;
                                    edificio="asilo";
                                    recursoSeleccionado = 11;
                                    creacionEdificio();
                                }
                            }
                        }
                    }
                    if(k!=x){
                        let n;
                        if(x<2){
                            n=0;
                        }else{
                            n=x-2
                        }                        
                        for(let m=n; m<=x+2; m++){
                            if(m<4){
                            if((matriz[m][y] == 5 || matriz[m][y] == 4) && (m!=k && m!=x) && (m!=k-3 && m!=k+3) ){
                                matrizPosicion[2][0] = m;
                                matrizPosicion[2][1] = y;
                                matrizRecurso[2] = matriz[m][y];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 14 &&(matrizRecurso[0]==4 || matrizRecurso[2]==4)){
                                    edificio = "asilo";
                                    recursoSeleccionado = 11;
                                    creacionEdificio();
                                    l=y+2;
                                }
                            }
                        }
                       }
                    }
                }
            } 
        }
    }
}
}

function taverna(){
if(edificioCreado==false){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];    
    matrizPosicion = new Array(3);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    let matrizRecurso = new Array(3);
    matrizRecurso[0] = recursoSeleccionado;
    let suma=0;
    
    let n;
    if(x==0){
        n=x;
    }else{
        n=x-1;
    }
    let m;
    if(x==3){
        m=3;
    }else{
        m=x+1;
    }
     
    for(let k=n; k<=m; k++){
        for(let l=y-1; l<=y+1; l++){
            if((k==x-1 && l==y) || (k==x && l!=y) || (k==x+1 && l==y)){
                if(matriz[k][l]==3 || matriz[k][l]==4){
                    matrizPosicion[1][0] = k;
                    matrizPosicion[1][1] = l;
                    matrizRecurso[1] = matriz[k][l]; 
                    if(k==x){
                        let r;
                        if(y<2){
                            r=0;
                        }else{
                            r=y-2;
                        }
                        for(let m=r; m<=y+2; m++){
                            if((matriz[x][m] == 3 || matriz[x][m] == 4) && (m!=l && m!=y) && (m!=l-3 && m!=l+3)){
                                matrizPosicion[2][0] = x;
                                matrizPosicion[2][1] = m;
                                matrizRecurso[2] = matriz[x][m];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 10 && (matrizRecurso[0]==4 || matrizRecurso[2]==4)){
                                    l=y+2;
                                    edificio="taberna";
                                    recursoSeleccionado = 8;
                                    creacionEdificio();
                                }
                            }
                        }
                    }
                    if(k!=x){
                        let n;
                        if(x<2){
                            n=0;
                        }else{
                            n=x-2
                        }                        
                        for(let m=n; m<=x+2; m++){
                            if(m<4){
                            if((matriz[m][y] == 3 || matriz[m][y] == 4) && (m!=k && m!=x) && (m!=k-3 && m!=k+3)){
                                matrizPosicion[2][0] = m;
                                matrizPosicion[2][1] = y;
                                matrizRecurso[2] = matriz[m][y];
                                if(matrizRecurso[0] + matrizRecurso[1] + matrizRecurso[2] == 10 && (matrizRecurso[0]==4 || matrizRecurso[2]==4)){
                                    edificio = "taberna";
                                    recursoSeleccionado = 8;
                                    creacionEdificio();
                                    l=y+2;
                                }
                            }
                        }
                      }
                    }
                }
            } 
        }
    }
 }                             
}

function pozo(){
if(edificioCreado == false){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];    
    matrizPosicion = new Array(2);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    let matrizRecurso = new Array(2);
    matrizRecurso[0] = recursoSeleccionado;
    
    let n;
    if(x==0){
        n=x;
    }else{
        n=x-1;
    }
    let m;
    if(x==3){
        m=3;
    }else{
        m=x+1;
    }
    
    for(let k=n; k<=m; k++){
        for(let l=y-1; l<=y+1; l++){
            if((k==x-1 && l==y) || (k==x && l!=y) || (k==x+1 && l==y)){
                if(matriz[k][l]==1 || matriz[k][l]==5){
                    matrizPosicion[1][0] = k;
                    matrizPosicion[1][1] = l;
                    matrizRecurso[1] = matriz[k][l]; 
                                if(matrizRecurso[0] + matrizRecurso[1] == 6){
                                    edificio = "pozo";
                                    recursoSeleccionado = 12;
                                    creacionEdificio();
                                    l=y+2;
                                }
                  }          
            }
        } 
    } 
}
}

function catedral(){
    if(edificioCreado == false && monuCatedral == true){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];
    matrizPosicion = new Array(3);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    let matrizRecurso = new Array(3);
    matrizRecurso [0] = recursoSeleccionado; 

    let n;
    if(x==0){
        n=x;
    }else{
        n=x-1;
    }
    let m;
    if(x==3){
        m=3;
    }else{
        m=x+1;
    }
    
    if(matrizRecurso[0] != 4){
      for(let k=n; k<=m; k++){
            if(k==x){
                k++;
            }
        if(k<4){  
        if(matriz[k][y]==2 || matriz[k][y]==4 || matriz[k][y]==5){
            matrizPosicion[1][0] = k;
            matrizPosicion[1][1] = y;
            matrizRecurso [1] = matriz[k][y];
            for(let l=y-1; l<=y+1; l++){
                if(l==y){
                    l++;
                }
                if((matriz[k][l] == 2 || matriz[k][l] == 4 || matriz[k][l] == 5) && edificioCreado==false){
                    matrizPosicion[2][0] = k;
                    matrizPosicion[2][1] = l;
                    matrizRecurso[2] = matriz[k][l];
                    if((matrizRecurso[0]==2 || matrizRecurso[0]==5) && matrizRecurso[1]==4 && (matrizRecurso[2]==2 || matrizRecurso[2]==5) 
                      && matrizRecurso[2]!=matrizRecurso[0]){
                         edificio = "Catedral de Caterina";
                         recursoSeleccionado = 20;
                         creacionEdificio();
                    }   
                }
            } 
        }
        
        if(k==x+1 && edificioCreado == false){
            for(let l=y-1; l<=y+1; l++){
                if(l==y){
                    l++;
                }
                if(matriz[x][l] == 2 || matriz[x][l] == 5 || matriz[x][l] == 4){
                    matrizPosicion[1][0] = x;
                    matrizPosicion[1][1] = l;
                    matrizRecurso[1] = matriz[x][l];
                    for(let p=n; p<=x+1; p++){
                        if(p==x){
                            p++;
                        }
                        if((matriz[p][l] == 2 || matriz[p][l] == 5 || matriz[p][l] == 4) && edificioCreado==false){
                            matrizPosicion[2][0] = p;
                            matrizPosicion[2][1] = l;
                            matrizRecurso[2] = matriz[p][l];
                            if((matrizRecurso[0]==5 || matrizRecurso[0]==2) && matrizRecurso[1]==4 && (matrizRecurso[2]==2 ||matrizRecurso[2]==5) && matrizRecurso[2]!=matrizRecurso[0]){
                                 edificio = "Catedral de Caterina";
                                 recursoSeleccionado = 20;
                                 creacionEdificio();
                            } 
                        }
                    }
                }
            }
        }
       }
      }
    }
    else{        
        for(let k=n; k<=m; k++){
            for(let l=y-1; l<=y+1; l++){
                if( (k==x-1 && l==y) || (k==x && l==y-1) || (k==x && l==y+1) || (k==x+1 && l==y)  ){
                    if(matriz[k][l] == 2 || matriz[k][l] == 5){
                        matrizPosicion[1][0] = k;
                        matrizPosicion[1][1] = l;
                        matrizRecurso[1] = matriz[k][l];
                        if(k!=x){
                            for(let p=y-1; p<=y+1; p++){
                                if((matriz[x][p] == 2 || matriz[x][p]==5)  && p!=y){
                                    matrizPosicion[2][0] = x;
                                    matrizPosicion[2][1] = p;
                                    matrizRecurso[2] = matriz[x][p];
                                    if(matrizRecurso[1] != matrizRecurso[2]){
                                        edificio = "Catedral de Caterina";
                                        recursoSeleccionado = 20;
                                        creacionEdificio();
                                        l=y+2;
                                        k=x+2;
                                    }
                                }
                            }
                        }else{
                            for(let p=n; p<=m; p++){
                                if((matriz[p][y] == 2 || matriz[p][y] == 5) && p!=x && edificioCreado==false){
                                    matrizPosicion[2][0] = p;
                                    matrizPosicion[2][1] = y;
                                    matrizRecurso[2] = matriz[p][y];
                                    if(matrizRecurso[1] != matrizRecurso[2]){
                                        edificio = "Catedral de Caterina";
                                        recursoSeleccionado = 20;
                                        creacionEdificio();   
                                        l=y+2;
                                        k=x+2;
                                    }
                                }
                            }
                          } 
                        }
                    }
                }
        }
    }
}
}

function palacio(){
    if(edificioCreado==false && monuPalacio == true){
    let x = matrizCeldaSeleccionada[0];
    let y = matrizCeldaSeleccionada[1];
    
    matrizPosicion = new Array(4);
    matrizPosicion [0] = [x,y];
    matrizPosicion [1] = [0,0];
    matrizPosicion [2] = [0,0];
    matrizPosicion [3] = [0,0];
    
    let matrizRecurso = new Array(4);
    matrizRecurso [0] = recursoSeleccionado; 

    let posicionCorrecta = false;
    let suma=0;
    
    for(let k=y-1; k<=y+1; k++){
        if(k==-1){
            k=1;
        }
        
        if(k==y){
            k++;
        }
        if(matriz[x][k] == 1 || matriz[x][k] == 2 || matriz[x][k] == 3 || matriz[x][k] == 4){
            matrizPosicion [1][0] = x;
            matrizPosicion [1][1] = k;
            matrizRecurso [1] = matriz[x][k];
            let n;
            if(x==0){
                n=x;
            }else{
                n=x-1;
            }
            let m;
            if(x==3){
                m=3;
            }else{
                m=x+1;
            }
            
            for(let l=n; l<=m; l++){
                if(l==x){
                        l++;
                    }
                if(l==-1){
                    l==1;
                }
            if(l<4){           
                if(matriz[l][k] == 1 || matriz[l][k] == 2 || matriz[l][k] == 3 || matriz[l][k] == 4){
                    matrizPosicion[2][0] = l;
                    matrizPosicion[2][1] = k;
                    matrizRecurso [2] = matriz[l][k];

                    if(matriz[l][y]==1 || matriz[l][y]==2 || matriz[l][y] == 3 || matriz[l][y] == 4){
                        matrizPosicion[3][0] = l;
                        matrizPosicion[3][1] = y;
                        matrizRecurso[3] = matriz[l][y];
                        
                        for(let m=0; m<=3 ; m++){
                            suma += matrizRecurso[m];
                        }
                        
                        if((matrizRecurso[0]!=matrizRecurso[1] && 
                           matrizRecurso[0]!=matrizRecurso[2] && 
                           matrizRecurso[0]!=matrizRecurso[3] && 
                           matrizRecurso[1]!=matrizRecurso[2] && 
                           matrizRecurso[1]!=matrizRecurso[3] &&
                           matrizRecurso[2]!=matrizRecurso[3]) &&
                           (matrizRecurso[0]+matrizRecurso[2]==3
                            || matrizRecurso[0]+matrizRecurso[2]==7)
                          ){
                            posicionCorrecta=true;
                        }
                            
                        if(suma==10 && posicionCorrecta==true){
                            edificio = "Palacio";
                            recursoSeleccionado = 21;
                            creacionEdificio();
                            suma=0;
                            posicionCorrecta=false;
                        }
                    }
                }
            }
             }      
           }
    }
 }
}

function seleccionMonumento(celda){
    if(blnGameOver==true){
        alert("Presiona iniciar juego");
    }else if(monuCatedral == false && monuPalacio==false){
        if(celda.id == 'catedral'){
            alert("elegiste catedral");
            monuCatedral=true;
        }
        else{
            alert("elegiste palacio");
            monuPalacio=true;
        }
        mostrarRecursos();
    }
}

function gameOver(){
    for(let i=0; i<=3; i++){
        for(let j=0; j<=3; j++){
            if(matriz[i][j] == 0){
                blnGameOver=false;
                j=4;
                i=4;
            }else{
                blnGameOver=true;
            }
        }
    }
    
    if(blnGameOver==true){
        alert("has perdido");
        document.getElementById('tabla').disabled=true;
        contarPuntos();
    }else{
    }
}

function contarPuntos(){
    let ptsCabaña=0;
    let granja=0;
    let ptsPosada=0;
    let blnPosada;
    let ptsSalaBanquete=0;
    let asilo=0;
    let ptsAsilo=0;
    let taberna = 0;
    let ptsTaberna=0;
    let ptsPozo=0;
    let alimento=0;
    let recursonegativo=0;
    let ptsTotales = 0;
    let puntajeFinal = 0;
    let blnCatedral = false;
    let ptsPalacio = 0;
    let satp=0;  
    let fpsp=0;
    let g=0;
    let c=0;
    
    for(let i=0; i<=3; i++){
        for(let j=0; j<=3; j++){
            if(matriz[i][j] < 6){
                recursonegativo+=1;    
            }
            if(matriz[i][j] == 7){
                granja += 1;
                alimento = granja*4;
            }
        }
    }
    
    for(let i=0; i<=3; i++){
        for(let j=0; j<=3; j++){
            switch(matriz[i][j]){
                case 6:
                    if(alimento>0){
                        ptsCabaña += 3;
                        alimento = alimento-1;
                    }
                    break;
                case 8:
                    taberna +=1;
                    ptsTaberna = ((taberna*taberna)/2) + ((taberna*3)/2);
                    break;
                case 9:
                    for(let k=0; k<=3; k++){
                        if(k==i){
                            for(let l=0; l<=3; l++){
                                if(matriz[k][l] == 8 || matriz[k][l] == 10 || matriz[k][l] ==11){
                                    blnPosada = false;
                                    l=4;
                                    k=4;
                                }
                                else{
                                    blnPosada = true;
                                }
                            }
                        }
                        if(k == j){
                            for(let l=0; l<=3; l++){
                                if(matriz[l][k] == 8 || matriz[l][k] == 10 || matriz[l][k] ==11){
                                    blnPosada = false;
                                    l=4;
                                    k=4;
                                }else{
                                    blnPosada = true;
                                }
                            }
                        }
                    }
                    if(blnPosada==true){
                            ptsPosada += 3;
                        }
                    break;
                case 10:
                    ptsSalaBanquete +=2;
                    break;
                case 11:
                    asilo +=1;
                    if(asilo == 1 || asilo == 3 || asilo == 5){
                        ptsAsilo = -asilo;
                    }else{
                        if(asilo>6){
                            ptsAsilo = 26;
                        }else{
                            switch(asilo){
                                case 2:
                                    ptsAsilo=5;
                                    break;
                                case 4:
                                    ptsAsilo=15;
                                    break;
                                case 6:
                                    ptsAsilo=26;
                                    break;
                            }
                        }
                    }
                    break;
                case 12:
                    let m;
                    if(i==0){
                        m=0;
                    }else{
                        m=i-1;
                    }
                    for(let k=m; k<=i+1; k++){
                        if(k<4){
                        for(let l=j-1; l<=j+1; l++){
                            if((k==i-1 && l==j) || (k==i && l==j-1) || (k==i && l==j+1) || (k==i+1 && l==j)){
                                if(matriz[k][l]==6){
                                    ptsPozo += 1;
                                }
                            }
                        }
                        }
                    }
                    break;
                case 20:
                    recursonegativo = 0;
                    break;
                case 21:
                    let z;
                    if(i==0){
                        z=0;
                    }else{
                        z=i-1;
                    }
                    for(let k=z; k<=i+1; k++){
                        if(k<4){
                            for(let l=j-1; l<=j+1; l++){
                                if((k==i-1 && l==j) || (k==i && l==j-1) || (k==i && l==j+1) || (k==i+1 && l==j)){
                                    if(matriz[k][l]==8 || matriz[k][l]==9 || matriz[k][l]==10 || matriz[k][l]==11){
                                        satp +=1;
                                        if(satp==1){
                                            ptsPalacio +=2;
                                        }
                                    }
                                    if(matriz[k][l]==12){
                                        fpsp+=1
                                        if(fpsp==1){
                                            ptsPalacio+=2;
                                        }
                                    }
                                    if(matriz[k][l]==7){
                                        g +=1;
                                        if(g==1){
                                            ptsPalacio+=2;
                                        }
                                    }
                                    if(matriz[k][l]==6){
                                        c+=1;
                                        if(c==1){
                                            ptsPalacio+=2;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;                    
            }
        }
    }
    ptsTotales = ptsCabaña + ptsPosada + ptsSalaBanquete + ptsAsilo + ptsTaberna + ptsPozo +ptsPalacio;
    puntajeFinal = ptsTotales - recursonegativo;
    
    if(puntajeFinal<=9){
        alert("El puntaje final es de: "+puntajeFinal+"\n Sigue intentando, aspirante a Arquitecto");
    }else if(puntajeFinal<=17){
        alert("El puntaje final es de: "+puntajeFinal+"\n Quizá a la próxima tengas más suerte, aprendiz de constructor");
    }else if(puntajeFinal<=24){
        alert("El puntaje final es de: "+puntajeFinal+"\n Buen intento, carpintero");
    }else if(puntajeFinal<=31){
        alert("El puntaje final es de: "+puntajeFinal+"\n ¡Felicidades, ingeniero!");
    }else if(puntajeFinal<=37){
        alert("El puntaje final es de: "+puntajeFinal+"\n ¡Sorprendente, todo un urbanista!");
    }else if(puntajeFinal>=38){
        alert("El puntaje final es de: "+puntajeFinal+"\n ¡Excelente juego, maestro!");
             }
}